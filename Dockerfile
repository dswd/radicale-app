FROM cloudron/base:0.10.0
MAINTAINER Johannes Zellner <johannes@cloudron.io>

RUN mkdir -p /app/code
WORKDIR /app/code

RUN apt-get update && apt-get install -y python3-dev python3-pip python3-setuptools python3-wheel && rm -rf /var/cache/apt /var/lib/apt/lists

# RUN python3 -m pip install radicale==2.1.5
# Use our own fork, until https://github.com/Kozea/Radicale/pull/699 is merged
RUN curl -L https://github.com/cloudron-io/Radicale/archive/ee3b7223d37355ce93b3e399c46d4adafed476d9.tar.gz  | tar -xz --strip-components 1 -f -
RUN python3 -m pip install . --upgrade

ADD config start.sh rights.default /app/code/

CMD [ "/app/code/start.sh" ]
