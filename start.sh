#!/bin/bash

set -eu -o pipefail

echo "=========================="
echo "     Radicale startup     "
echo "=========================="

echo "=> Update radicale config"
sed -e "s/ldap_url = ldap:\/\/.*/ldap_url = ldap:\/\/${LDAP_SERVER}:${LDAP_PORT}/" \
    -e "s/ldap_base = .*/ldap_base = ${LDAP_USERS_BASE_DN}/" \
    -e "s/ldap_binddn = .*/ldap_binddn = ${LDAP_BIND_DN}/" \
    -e "s/ldap_password = .*/ldap_password = ${LDAP_BIND_PASSWORD}/" \
    "/app/code/config" > "/run/config"

if ! [ -e /app/data/rights ]; then
    echo "=> Setting default rights"
    cp /app/code/rights.default /app/data/rights
fi

echo "=> Ensure folder permissions"
chown -R cloudron:cloudron /app/data

echo "=> Start radicale"
exec /usr/local/bin/gosu cloudron:cloudron /app/code/bin/radicale --debug --config /run/config
