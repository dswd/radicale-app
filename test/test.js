#!/usr/bin/env node

'use strict';

require('chromedriver');

var execSync = require('child_process').execSync,
    ejs = require('ejs'),
    expect = require('expect.js'),
    fs = require('fs'),
    url = require('url'),
    mkdirp = require('mkdirp'),
    path = require('path'),
    rimraf = require('rimraf'),
    superagent = require('superagent'),
    webdriver = require('selenium-webdriver');

var by = webdriver.By,
Keys = webdriver.Key,
until = webdriver.until;

var VDIRSYNCER = process.env.VDIRSYNCER || 'vdirsyncer';

console.log('===============================================');
console.log(' This test requires vdirsyncer to be installed (using ' + VDIRSYNCER + ' )');
console.log('===============================================');

if (!process.env.USERNAME || !process.env.PASSWORD) {
    console.log('USERNAME and PASSWORD env vars need to be set');
    process.exit(1);
}

describe('Application life cycle test', function () {
    this.timeout(0);

    var server;
    var browser = new (require('selenium-webdriver/chrome')).Driver();

    var LOCATION = 'test';
    var app;
    var collectionId;
    var TEST_TIMEOUT = 10000;
    var username = process.env.USERNAME;
    var password = process.env.PASSWORD;
    var collectionName = 'mycollection';

    before(function (done) {
        var seleniumJar= require('selenium-server-standalone-jar');
        var SeleniumServer = require('selenium-webdriver/remote').SeleniumServer;
        server = new SeleniumServer(seleniumJar.path, { port: 4444 });
        server.start();

        done();
    });

    after(function (done) {
        browser.quit();
        server.stop();
        done();
    });

    function waitForElement(elem) {
        return browser.wait(until.elementLocated(elem), TEST_TIMEOUT).then(function () {
            return browser.wait(until.elementIsVisible(browser.findElement(elem)), TEST_TIMEOUT);
        });
   }

    function login(callback) {
        browser.manage().deleteAllCookies().then(function () {
            return browser.get('https://' + app.fqdn + '/.web/');
        }).then(function () {
            return browser.sleep(5000);
        }).then(function () {
            return waitForElement(by.xpath('//input[@name="user"]'));
        }).then(function () {
            return browser.findElement(by.xpath('//input[@name="user"]')).sendKeys(username);
        }).then(function () {
            return browser.findElement(by.xpath('//input[@name="password"]')).sendKeys(password);
        }).then(function () {
            return browser.findElement(by.tagName('form')).submit();
        }).then(function () {
            return waitForElement(by.xpath('//h1[text()="Collections"]'));
        }).then(function () {
            callback();
        });
    }

    function logout(callback) {
        browser.get('https://' + app.fqdn + '/.web/').then(function () {
            return browser.sleep(5000);
        }).then(function () {
            return waitForElement(by.xpath('//h1[text()="Collections"]'));
        }).then(function () {
            return browser.findElement(by.xpath('//*[@id="logoutview"]/*[1]')).click();
        }).then(function () {
            return waitForElement(by.xpath('//input[@name="user"]'));
        }).then(function () {
            callback();
        });
    }

    function createCollection(callback) {
        browser.get('https://' + app.fqdn + '/.web/').then(function () {
            return browser.sleep(5000);
        }).then(function () {
            return waitForElement(by.xpath('//a[@name="new"]'));
        }).then(function () {
            return browser.findElement(by.xpath('//a[@name="new"]')).click();
        }).then(function () {
            return waitForElement(by.xpath('//section[@id="createcollectionscene"]/*/input[@name="displayname"]'));
        }).then(function () {
            return browser.findElement(by.xpath('//section[@id="createcollectionscene"]/*/input[@name="displayname"]')).sendKeys(collectionName);
        }).then(function () {
            return browser.findElement(by.xpath('//section[@id="createcollectionscene"]/*/select[@name="type"]/option[1]')).click();   // select addressbook
        }).then(function () {
            return browser.findElement(by.xpath('//section[@id="createcollectionscene"]/*/button[@type="submit"]')).click();
        }).then(function () {
            return waitForElement(by.xpath('//span[text()="' + collectionName + '"]'));
        }).then(function () {
            callback();
        });
    }

    function getCollection(callback) {
        browser.get('https://' + app.fqdn + '/.web/').then(function () {
            return browser.sleep(5000);
        }).then(function () {
            return browser.findElement(by.xpath('//section[@id="collectionsscene"]/article[1]//a[1]')).getAttribute('href');
        }).then(function (href) {
console.dir(href);
            var tmp = url.parse(href);
            collectionId = tmp.path.split('/').filter(function (f) { return f; }).pop();

            console.log('Using collection: ', collectionId);

            callback();
        });
    }

    xit('build app', function () {
        execSync('cloudron build', { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });

    it('install app', function () {
        execSync('cloudron install --new --location ' + LOCATION, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });

    it('can get app information', function () {
        var inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location === LOCATION; })[0];
        expect(app).to.be.an('object');
    });

    it('can login', login);
    it('can create collection', createCollection);
    it('can get collection id', getCollection);
    it('can logout', logout);

    it('can sync addressbook', function () {
        rimraf.sync('/tmp/vdirsyncer');

        var statusPath = '/tmp/vdirsyncer/status/';
        var addressBookPath = '/tmp/vdirsyncer/addressbook/';
        var testContactFile = __dirname + '/test_contact.vcf';

        mkdirp.sync(statusPath);
        mkdirp.sync(addressBookPath);

        var configFile = '/tmp/vdirsyncer/config';
        var data = {
            statusPath: statusPath,
            addressBookPath: addressBookPath,
            cardDavUrl: 'https://' + app.fqdn + '/' + username + '/' + collectionId + '/',
            username: username,
            password: password
        };

        fs.writeFileSync(configFile, ejs.render(fs.readFileSync('vdirsyncer.conf.ejs', 'utf-8'), data));
        fs.writeFileSync(addressBookPath + 'test_contact.vcf', fs.readFileSync(testContactFile));

        var env = process.env;
        env.VDIRSYNCER_CONFIG = configFile;
        execSync(VDIRSYNCER + ' discover ', { env: env, stdio: 'inherit' });
        execSync(VDIRSYNCER + ' sync', { env: env, stdio: 'inherit' });

        expect(fs.readFileSync(testContactFile, 'utf-8')).to.equal(fs.readFileSync(addressBookPath + 'test_contact.vcf', 'utf-8'));
    });

    it('backup app', function () {
        execSync('cloudron backup create --app ' + app.id, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });

    it('restore app', function () {
        execSync('cloudron restore --app ' + app.id, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });

    it('can login', login);
    it('can get collection id', getCollection);
    it('can logout', logout);

    it('addressbook is still ok', function () {
        rimraf.sync('/tmp/vdirsyncer');

        var statusPath = '/tmp/vdirsyncer/status/';
        var addressBookPath = '/tmp/vdirsyncer/addressbook/';
        var testContactFile = __dirname + '/test_contact.vcf';

        mkdirp.sync(statusPath);
        mkdirp.sync(addressBookPath);

        var configFile = '/tmp/vdirsyncer/config';
        var data = {
            statusPath: statusPath,
            addressBookPath: addressBookPath,
            cardDavUrl: 'https://' + app.fqdn + '/' + username + '/' + collectionId + '/',
            username: username,
            password: password
        };

        fs.writeFileSync(configFile, ejs.render(fs.readFileSync('vdirsyncer.conf.ejs', 'utf-8'), data));

        var env = process.env;
        env.VDIRSYNCER_CONFIG = configFile;
        console.log('VDIRSYNCER_CONFIG=/tmp/vdirsyncer/config vdirsyncer sync');
        execSync(VDIRSYNCER + ' discover test_contacts', { env: env, stdio: 'inherit' });
        execSync(VDIRSYNCER + ' sync', { env: env, stdio: 'inherit' });

        var contactRemoteFileName = fs.readdirSync(addressBookPath)[0];
        expect(contactRemoteFileName).to.be.a('string');

        var testContactFileContent = fs.readFileSync(testContactFile, 'utf-8');
        var testContactRemoteFileContent = fs.readFileSync(addressBookPath + '/' + contactRemoteFileName, 'utf-8');

        // remove the dynamic radicale id
        testContactRemoteFileContent = testContactRemoteFileContent.replace('X-RADICALE-NAME:' + contactRemoteFileName + '\n', '');

        expect(testContactFileContent).to.equal(testContactRemoteFileContent);
    });

    it('can restart app', function (done) {
        execSync('cloudron restart --wait --app ' + app.id);
        done();
    });

    it('can login', login);
    it('can get collection id', getCollection);
    it('can logout', logout);

    it('uninstall app', function () {
        execSync('cloudron uninstall --app ' + app.id, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });
});
